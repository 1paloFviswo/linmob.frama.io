+++
title = "The Galaxy Nexus and Android 4.0:  A Short Comment"
aliases = ["2011/10/galaxy-nexus-and-android-40-short.html"]
date = "2011-10-19T11:28:00Z"
[taxonomies]
tags = ["Android", "Android 4.0 Ice Cream Sandwich", "flagship devices", "Galaxy Nexus", "smartphone", "super phone", "tablet", "TI OMAP 4"]
categories = ["commentary", "software"]
authors = ["peter"]
+++
Early today, at 9:00 am Hongkong time, the postponed Launch of the Samsung made Google Galaxy Nexus superphone, which is a awesome flagship device for the Android platform, featuring a 4.65 inch curved touchscreen which offers you an amazing real 720p resolution (not much less than the resolution your cheap 15.6&#8221; notebook offers). Inside, there is a TI OMAP 4460 dual core chip running at 1.2&#160;GHz powering the slab device to likely pretty amazing speeds&mdash;on the outside, there are no more capacitive buttons, strongly reminding of the Homeycomb tablets out there.
<!-- more -->
The real innovation IMHO happened on the software side of things. Android 4.0 apparently solves at least some of the usability issues that have been annoying me for a while, to just mention one thing: multitasking seems to be hugely improved.

You may argue that Googles designers mostly just ripped of webOS and Nokia's Swipe UI, and in some certain ways, there are certain resemblances of what we've seen on the aforementioned platforms before, but all together, Google reiterated on that, and while they brought in gestures, they did it in a way that really fits into what Android has been like before in order not to scare away existing Android users.

Well, maybe it's not all that overwhelming as I think now, writing this article during my lunch break after having had a look at thisismynext.com's coverage of this event&mdash;so you better have a look at what these people wrote, photographed and filmed for us all, before I am going to extend this article in the evening (European time).

<b>SOURCE:</b> <a href="http://thisismynext.com/tag/galaxy-nexus-2011/" target="_blank">thisismynext.com</a>
