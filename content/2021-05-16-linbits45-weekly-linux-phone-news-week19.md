+++
title = "LinBits 45: Weekly Linux Phone news / media roundup (week 19)"
aliases = ["2021/05/16/linbits45-weekly-linux-phone-news-week19.html"]
date = "2021-05-16T21:59:00Z"
[taxonomies]
tags = ["PINE64", "PinePhone",  "LINMOBapps", "postmarketOS",  "Mobian", "Ubuntu Touch", "Manjaro", "Sailfish OS", "Maui", "PINE64 Community Update", "AlpineConf", "Linux App Summit",]
categories = ["weekly update"]
authors = ["peter"]
+++

_It's sunday. Now what happened since last sunday?_

Ubuntu Touch OTA 17, Sailfish OS 4.1, great postmarketOS content from AlpineConf and more!<!-- more --> _Commentary in italics._


### Software development and releases
* Manjaro Phosh Beta 9 [has been released](https://twitter.com/ManjaroLinux/status/1391646519082823681). It features all the new software releases we had in last LinBits and more.
* UBPorts: [Ubuntu Touch OTA-17 Release](https://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-17-release-3755). _Please note: The PinePhone is not running on the OTA schedule!_
* Jolla have pre-released [Sailfish OS 4.1](https://forum.sailfishos.org/t/release-notes-kvarken-4-1-0/5942).

### Worth noting
* [Manjaro have switched their latest dev images for Phosh and Plasma Mobile over to Pipewire as an audio server](https://gitlab.manjaro.org/manjaro-arm/issues/pinephone/phosh/-/issues/171). Testing is welcome! _Thanks to [Jan Jasper de Kroon for telling me about this](https://twitter.com/jajadekroon/status/1393697501451112457)!_
* The [Mobian Wiki](https://wiki.mobian-project.org/doku.php?id=firefox-esr) has interesting new information on customizing Firefox ESR so that it has the URL bar at the bottom.

### Worth reading 
* geekyschmidt: [Open Source On the Go](https://geekyschmidt.com/personal/tech/linux/phones/2021/05/11/linuxphones.html). _Nice little PinePhone review with a bit of history!_
* Plasma Mobile: [Plasma Mobile Gear May 2021](https://www.plasma-mobile.org/2021/05/10/plasma-mobile-gear-may-2021/?utm_source=atom_feed). _Gear is the new (slightly strange) name for monthly releases of KDE apps._
* FOSS2go: [Manjaro ARM Beta9 of Phosh for PinePhone](https://foss2go.com/manjaro-arm-beta9-of-phosh-for-pinephone/)
* NXos: [Maui 1.2.2 Release](https://nxos.org/maui/maui-1-2-2-release/). _A new release of the awesome Maui apps._
* LinuxSmartphones: [Guest Post: In Linux smartphones we trust](https://linuxsmartphones.com/guest-post-in-linux-smartphones-we-trust/). _Great guest post by PINE64 Community Manager, Lukasz Erecinski. Please note that LinuxSmartphones.com uses Cloudflare, so don't click these links if you have concerns about that._
* LinuxSmartphones: [Sailfish OS 4.1 brings new apps, features, and bug fixes, as support ends for the Jolla Phone](https://linuxsmartphones.com/sailfish-os-4-1-brings-new-apps-features-and-bug-fixes-as-support-ends-for-the-jolla-phone/). _Brad's take on the announcement by Jolla._
* PINE64: [May Update: Connection Established](https://www.pine64.org/2021/05/15/may-update-connection-established/). _Another great update by PINE64. The PinePhone availablility for 2021 has been secured, some new accessories will be availble in June, and the clamshell keyboard will be available later this summer._
  * LinuxSmartphones: [PinePhone can now be purchased year round, wireless charging and LoRa cases coming in June, keyboard case coming this summer](https://linuxsmartphones.com/pinephone-can-now-be-purchased-year-round-wireless-charging-and-lora-cases-coming-in-june-keyboard-case-coming-this-summer/). _Brad's take._

### Worth listening

### Worth watching

* PINE64: [May Update: Connection Established](https://odysee.com/@PINE64:a/may-update-insert-title-here:5). _Nice video synopsis by PizzaLovingNerd!_
* Terry Denney: [Pinephone unboxing](https://www.youtube.com/watch?v=BSH1uxSX3jw). _It's a box. There's a PinePhone in it :-)_
* Sk4zZi0uS: [Its PineTime - Infinitime 1.0.0 Impressions with PinePhone 2021-05-13](https://odysee.com/@Sk4zZi0uS:0/Its-PineTime---Infinitime-1.0.0-Impressions-with-PinePhone-2021-05-13:2). _Sk4zZi0uS has a PineTime now!_

#### Hardware mod corner
* silver: [3dppkb](https://vimeo.com/549127315). _silver build a nice adapter for a tablet keyboard. Looking good!_
* NULL: [PinePhone Mods Speed test](https://www.youtube.com/watch?v=7Hnxp9RE7H0). _This is quite the performance increase, although it seems to show mostly on benchmarks._
* Pako St: [PinePhone Flutter Wayland on NemoMobile Manjaro](https://www.youtube.com/watch?v=JRcDmSuby6s). _You could just use that Flatpak from Flathub, but that would be to easy!_

#### Software corner
* Leszek Lesner: [SailfishOS 4.1 - Whats new!?](https://www.youtube.com/watch?v=9nWGoH3TkkUI) _Great overview video by Leszek!_
* Niccolò Ve: [MAUI Applications: File Manager, Image Viewers, and So Many Consistency, Convergency, and Beauty!](https://www.youtube.com/watch?v=CipvQWi6aNQ) _These Maui apps are nice!_


#### Ubuntu Touch Corner
* Linux Lounge: [Ubuntu Touch OTA 17 Released!](https://odysee.com/@LinuxLounge:b/ubuntu-touch-ota-17-released!:1) _A video about the announcement linked above!_
* The Linux Experiment: [Ubuntu Touch on the Pinephone - is this the best Linux mobile interface?](https://www.youtube.com/watch?v=LVkOWclo52c) _Great look at Ubuntu Touch on the PinePhone!_

#### AlpineConf Corner
* Martijn Braam: [AlpineConf - Oliver - pmbootstrap: the Swiss Army knife of postmarketOS development](https://www.youtube.com/watch?v=WH0SU0eRSbA). _pmbootstrap is an amazing tool! Make sure to watch this!_
* Martijn Braam: [AlpineConf - Martijn Braam - Showing off postmarketOS](https://www.youtube.com/watch?v=npKwMWr1F_0). _Great talk by Martijn!_
* Proycon: [Sxmo: Simple X Mobile - A minimalist environment for Linux smartphones](https://diode.zone/videos/watch/3f0948cf-47df-437e-b1ea-76fec58479c2). _Great demo of Sxmo!_

#### Linux App Summit Corner

* Jing OS: [Jingling Tech introducing JingPad A1 on Linux App Submit(LAS) 2021 (Specs and pricing included)](https://www.youtube.com/watch?v=qqHb-xXHskk)
* Tobias Bernard: [Adaptive Apps: The Future is Now](https://youtu.be/1pFADheIzQk?t=1006)

_There was a lot more going on that I did not manage to watch yet, see [here for now](https://conf.linuxappsummit.org/event/3/timetable/#all)._

### Stuff I did

#### Content
None, sorry.

#### LINMOBapps
I did a lot of work on LINMOBapps this week, again, if a little less than last week. It's replacement now has a [Twitter account](https://twitter.com/linuxphoneapps). Two new colums have been added, detailing the main project programming language and the build system, information targeted mainly at developers and distributors, filling of these columns will likely take a while. I also added the following apps this week: 
* [SciteQt](https://fosstodon.org/@linmob/106218566210927301), a Scintilla editor written in QtQuick,
* five Gemini browsers:
  * [Moonlander](https://twitter.com/linuxphoneapps/status/1392927158016909315),
  * [Fossil](https://twitter.com/linuxphoneapps/status/1392929056564383762),
  * [Readerview](https://twitter.com/linuxphoneapps/status/1392920004442591233),
  * [alrisha](https://twitter.com/linuxphoneapps/status/1392870042501632001) and
  * [Rocket](https://twitter.com/linuxphoneapps/status/1393885172228136962),
* [Agregore Browser](https://twitter.com/linuxphoneapps/status/1393890082172686336), a browser for the distributed web.
Thanks to _cahfofpai_ for contributing [Tok](https://invent.kde.org/network/tok), a WIP Kirigami Telegram client.
[A lot else here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). 
Please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
