+++
title = "Weekly GNU-like Mobile Linux Update (28/2022): New Manjaro betas, Sailfish OS supporting VoLTE and RkVDEC progress"
date = "2022-07-17T19:20:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","BM818 Modem Firmware","Sailfish OS","VoLTE","PinePhone Pro","Manjaro","Google Pixel 3",]
categories = ["weekly update"]
authors = ["peter"]
+++
PinePhone Keyboard hacks, a nice FSFE interview, and a far to long comment on a video. _I blame YouTube._
<!-- more -->
_Commentary in italics._

### Software progress
#### GNOME ecosystem
* This Week in GNOME: [#52 Happy Birthday!](https://thisweek.gnome.org/posts/2022/07/twig-52/). _Happy Anniversary! Also, a lot of good stuff in here, e.g., about Nautilus, Dialect and Blurble!_
* chergert: [Builder 43.alpha0](https://blogs.gnome.org/chergert/2022/07/14/builder-43-alpha0/). _Well done!_

#### Plasma/Maui ecosystem
* Nate Graham: [This week in KDE: some nice improvements!](https://pointieststick.com/2022/07/15/this-week-in-kde-some-nice-improvements/). _Nice improvements!_
* KDE Announcements: [KDE Plasma 5.25.3, Bugfix Release for July](https://kde.org/announcements/plasma/5/5.25.3/).
* snehit: [Review and Updates for Merge Request - GSoC'22 post #7](https://snehit.dev/posts/kde/gsoc-22/merge-request-review/). _This is about NeoChat._

#### Ubuntu Touch
* UBports: [Ubuntu Touch Q&A 119](https://ubports.com/de/blog/ubports-blogs-nachrichten-1/post/ubuntu-touch-q-a-119-3858). _We had the video earlier, now transcript and audio-only are available!_

#### Sailfish OS
* flypig: [Sailfish Community News, 14th July, Sailfish OS 4.4.0.68 - Community News](https://forum.sailfishos.org/t/sailfish-community-news-14th-july-sailfish-os-4-4-0-68/12289). _Nice round-up. As someone who really should revisit Sailfish OS, the Whisperfish progress is most exciting!_
* Nico Cartron: [VoLTE on Sailfish OS / Xperia 10 II](https://www.ncartron.org/volte-on-sailfish-os--xperia-10-ii.html). _VoLTE is one of these hurdles Mobile Linux products absolutely need to pass to stay relevant, and I am really glad to see that Sailfish OS is doing well here!_

#### Distributions
* [Manjaro ARM Beta25 with Phosh (PinePhone / PinePhonePro)](https://forum.manjaro.org/t/manjaro-arm-beta25-with-phosh-pinephone-pinephonepro/116529) has been released, updating things to current releases. _[Caveats apply](https://fosstodon.org/@calebccff/108645930580968593)._
* [Manjaro ARM Beta 12 with Plasma Mobile (PinePhone / PinePhone Pro)](https://forum.manjaro.org/t/manjaro-arm-beta-12-with-plasma-mobile-pinephone-pinephone-pro/116373) was also released, updating the default PinePhone (Pro) distribution to the current Plasma releases.

#### Low level stuff
* BayLibre: [Linux kernel 5.18 released, our contributions](https://baylibre.com/linux-kernel-5-18-released-our-contributions/). _Some news about SoC enablement!_
* Phoronix: [Rockchip RkVDEC Linux Driver Being Prepared For HEVC/H.265 Support](https://www.phoronix.com/scan.php?page=news_item&px=RkVDEC-HEVC-Patches). _Looking forward to this landing for the PinePhone Pro!_

### Worth noting
* amarok on Purism forums: [TUTORIAL: Enable Adwaita Dark Mode on the Librem 5](https://forums.puri.sm/t/tutorial-enable-adwaita-dark-mode-on-the-librem-5/17803). _I would rather use the mobile friendly [postmarketOS Tweaks](https://gitlab.com/postmarketOS/postmarketos-tweaks) app for this, or manually trigger the relatively new dark style preference setting by running `gsettings set org.gnome.desktop.interface color-scheme prefer-dark`, but the outlined way is more approachable._
* [Biktor (@biktorgj): "I couldn't leave all the @Puri_sm #Librem 5 users missing all the fun! Just getting started, Broadmobi BM818 Booting our custom bootloader and starting (and crashing mid boot) the Linux kernel from the modem distro!"](https://twitter.com/biktorgj/status/1548550623049576449#m). _Awesome!_
  * Purism forums: [L5 Modem BM818 - Open Firmware - Librem / Phones (Librem 5) - Purism community](https://forums.puri.sm/t/l5-modem-bm818-open-firmware/17810). _Related discussion thread._

#### PinePhone Keyboard tweaks and mods[^1]
* magdesign on PINE64 forums: [Convert PinePhone Keyboard into Powerbank](https://forum.pine64.org/showthread.php?tid=17015&pid=111514#pid111514).
* magdesign on PINE64 forums: [Keyboard drawing only 0.1A from charger](https://forum.pine64.org/showthread.php?tid=16979&pid=111414#pid111414).
* /u/i3craig: [Using a USB device with the PinePhone Keyboard attached - Oh Yea! [Keyboard modding instructions] : PINE64official](https://www.reddit.com/r/PINE64official/comments/vw4s8t/using_a_usb_device_with_the_pinephone_keyboard/).

### Worth reading
#### SqueakPhone
* Syndicated Actors: [State of the SqueakPhone](https://syndicate-lang.org/journal/2022/06/03/phone-progress). _I had not heard of this before and I am not sure I understand all the jargon, but it looks quite neat!

#### More on GNOME Calls
* Purism: [Voice over IP in GNOME Calls Part 2: The Implementation](https://puri.sm/posts/voice-over-ip-in-gnome-calls-part-2-the-implementation/). _Another nice post!_

#### Free Software Love Europe
* FSFE: [A PC in your pocket: Librem 5, a Free Software phone](https://fsfe.org/news/2022/news-20220712-01.html). _Nice interview!_

#### Old news
* Hackaday: [Open Firmware For PinePhone LTE Modem – What’s Up With That?](https://hackaday.com/2022/07/12/open-firmware-for-pinephone-lte-modem-whats-up-with-that/).
* It's FOSS News: [Nokia Targets An Amateur Linux Phone Project 'NOTKIA' for a Name Change](https://news.itsfoss.com/nokia-notkia/).

#### Fluff
* Purism: [Privacy in Depth](https://puri.sm/posts/privacy-in-depth/). _Despite the mean headline I picked for this, it may be worth a read. Also: The age of the photo of the Librem 5 camera may surprise you - Evergreen does not look like this!_

#### Not mobile, but important
* mjg59: [Responsible stewardship of the UEFI secure boot ecosystem](https://mjg59.dreamwidth.org/60248.html).

### Worth watching
#### CRISIS!?
* Ambro's: [Pinephone Pro July '22 Review - Mobile Linux Is Dying](https://www.youtube.com/watch?v=GSfR3uHbh60). _This warrants a longer comment, which I tried to post to YouTube... but... well, the automated moderation system did not approve._ <details><summary>So let's have that comment here.</summary>
First of all: I share some concerns: There's less activity and less hype on social media than there was a year ago. But: There are still more updates to existing apps and more new apps all the time, making it impossible for me to test and add them all to [LinuxPhoneApps](https://linuxphoneapps.org/) in time, and Purism are very much actively developing software for the Librem 5.<br>
__Further nitpicks:__ Manjaro don't develop software, they package it (see comment above). Arch Linux ARM is still going strong, Danct12 just did not see the need to create another image since May, which, as long as the distribution does not break on upgrades, is perfectly fine. Mobian and postmarketOS are both quite active, and the same goes for most projects I am aware of.<br>
__On PINE64 investing in software:__ I would very much like to see that too, especially for everything around hardware enablement and fixing hard to reproduce but common bugs (e.g., suspend issues). But: This would necessarily lead to way higher prices (doubling them, maybe), to make a meaningful difference.<br>
__Regarding PinePhone Pro battery life:__ I am disappointed with active use battery life too. While especially video playback lifetime should improve when using video playback solutions that offload video decoding from the CPU (meaning: likely not Firefox for a while), and there may be other options to improve power efficiency (e.g., disabling the big A72 cores when they are not specifically needed), these things take time, assuming that they are at all possible. I would have loved for PINE64 to alter the design more for the Pro in order to allow for a bigger battery, but I also understand that that would have been very costly. I just hope that PINE64 or the community will come up with solution for larger batteries (with a different back case), or more elegant (as in smaller) solutions for [external batteries](https://tilvids.com/w/w3qXbr31SE21jGxnM8A1r7) to solve this.<br>
__Conclusion:__ While I don't like the alarmist title too much, it's a good wake up call. If it helps people that expect parity with iOS or Android to not onboard mobile Linux at this point, then that's really good... unhappy non-constructive complainers are not needed. I agree that we need investment in software, if we want mobile Linux to thrive, but we must not wait for someone else to do it, we just need to contribute to key projects in money and time whenever we can.
</details>


#### Plasma Mobile Progress
* ItsMCB: [This feels AWESOME! - Manjaro Beta 12 with Plasma Mobile 5.25 First Impressions](https://www.youtube.com/watch?v=iv62fTdruL0). _Nice!_

#### Nautilus 43
* baby WOGUE: [Files port to Adwaita tabs, GNOME 43](https://www.youtube.com/watch?v=_QPl5Maq8pQ).

#### Hardware enablement
* caleb: [Pixel 3 running postmarketOS](https://fosstodon.org/@calebccff/108658098964536659).


#### Shorts
* Niper_YT: [LG Nexus 5 (hammerhead) with Sailfish OS 4.0.1.48 #shorts #jolla #sailfish](https://www.youtube.com/shorts/zOlz8D82TUM).
* CyberPunked: [Discord on Waydroid - Ubuntu Touch #shorts](https://www.youtube.com/shorts/lhe_t_VRGuE).

### Something missing?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)

_23_

[^1]: Thanks to [PocketVJ](https://fosstodon.org/@pocketvj/108661782956748758) for bringing these cool posts to my attention!
