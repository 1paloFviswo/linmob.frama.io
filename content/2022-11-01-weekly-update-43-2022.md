+++
title = "Weekly GNU-like Mobile Linux Update (43/2022): An impressive Gadget"
date = "2022-11-01T20:45:00Z"
draft = false
[taxonomies]
tags = ["SqueakPhone", "Clockwork uConsole", "Signal", "PinePhone", "Kirigami", "Mobile NixOS","Nemo Mobile",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = "and synQun (with friendly assistance from plata's awesome script)"
+++

Also: Progress reports by Mobian, Manjaro and Nemo Mobile, a nice video on SqueakPhone and a lot of the usual!
<!-- more -->

_Commentary in italics._

### Hardware
- [Clockwork uConsole](https://www.clockworkpi.com/uconsole): A 5" plus keyboard handheld device, wide choice of SoCs (Rockchip RK3399 (A-06), Broadcom BCM2711 (RPI-CM4 Lite), AllWinner H6 (A-04) or D1 (R01), an optional LTE module, starting at USD 139 - likely quite useless, but also a hell of a gadget. _So far I've managed to keep myself from pre-ordering one._
  - MobileLinux: [Clockwork's uConsole is a modular portable computer for $139 and up, RaspPi CM4 and 4G connection available as options](https://www.reddit.com/r/mobilelinux/comments/ydtcgf/clockworks_uconsole_is_a_modular_portable/)

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#67 File Descriptors and Scopes](https://thisweek.gnome.org/posts/2022/10/twig-67/)
- GTK blog: [On deprecations](https://blog.gtk.org/2022/10/30/on-deprecations/)
- Sébastien Wilmet: [Devhelp](https://informatique-libre.be/swilmet/blog/2022-10-23-devhelp.html)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: next-generation improvements](https://pointieststick.com/2022/10/28/this-week-in-kde-next-generation-improvements/)
- Volker Krause: [New KDE CI configuration options and Plasma KF6 CI](https://www.volkerkrause.eu/2022/10/29/kde-ci-fine-grained-platform-options.html)
- TSDgeos: [The KDE Qt5 Patch Collection has been rebased on top of Qt 5.15.7](https://tsdgeos.blogspot.com/2022/10/the-kde-qt5-patch-collection-has-been.html)
- Qt blog: [The Road to Qt Location](https://www.qt.io/blog/the-road-to-qt-location)
- KDE e.V.: [KDE e.V. elects new board members](https://ev.kde.org//2022/10/26/decisions/)
- KDE Announcements: [KDE Plasma 5.26.2, Bugfix Release for October](https://kde.org/announcements/plasma/5/5.26.2/)
- Carl Schwan: [Kirigami Addons 0.5 release](https://carlschwan.eu/2022/10/23/kirigami-addons-0.5-release/)

#### Nemo Mobile
- Nemo Mobile UX team: [Nemomobile in October 2022](https://nemomobile.net/pages/nemomobile-in-october-2022/)

#### Sailfish OS

#### Distributions
- Mobian Blog: [These... past 4 months in Mobian: July - October 2022](https://blog.mobian.org/posts/2022/11/01/tmim/)
- Breaking updates in pmOS edge: [Nokia N900: keyboard mapping broken](https://postmarketos.org/edge/2022/10/31/nokia-n900-broken-keymap/)
- glodroid_manifest (GitHub): [GloDroid v0.8.1](https://github.com/GloDroid/glodroid_manifest/releases/tag/v0.8.1)
- Manjaro PinePhone Plasma Mobile: [Beta 13](https://github.com/manjaro-pinephone/plasma-mobile/releases/tag/beta13) _5.26!_
- Manjaro Blog: [October 2022 in Manjaro ARM](http://blog.manjaro.org/2022/10/27/october-2022-in-manjaro-arm/)
- Mobile NixOS: [PinePhone got a flashable installer image that allows you to setup FDE and choose between Phosh or Plasma](https://mobile.nixos.org/devices/pine64-pinephone.html). _ALL CAPS NICE!_


#### Matrix
- Matrix.org: [This Week in Matrix 2022-10-28](https://matrix.org/blog/2022/10/28/this-week-in-matrix-2022-10-28)

### Worth noting
- [Guido Günther: "Devices with notches and rounded corners using #phosh so far have to tweak CSS to move the clock out of center (https://gitlab.gnome.org/World/Phosh/phosh/-/issues/552). I've experimented with a GSetting to just move the whole panel down a bit: https://gitlab.gnome.org/World/Phosh/phosh/-/merge_requests/1158](https://social.librem.one/@agx/109247185823254216)
- MobileLinux: [Running Ubuntu 22.10 w/ Kernel 6.0+ on my PinePhone Pro because why not.](https://www.reddit.com/r/mobilelinux/comments/ychbca/running_ubuntu_2210_w_kernel_60_on_my_pinephone/)
- r/PinePhone: [Not booting on Micro SD](https://www.reddit.com/r/pinephone/comments/yde4d9/not_booting_on_micro_sd/). _Not news, but maybe helpful for people using Tow-Boot on the OG PinePhone._
- r/PinePhoneOfficial: [PinePhone and LoRa?](https://www.reddit.com/r/PinePhoneOfficial/comments/yiehrc/pinephone_and_lora/). _If you've played with the PinePhone LoRa back-cover and have something to share, OP and I would certainly love to hear about it! (If you don't use reddit, you can use me as an email relay!)_
- [Ha-NeuerSoftware on Twitter: "Ha-NeuOS is a new OS for the #PinePhone Pro! It’s based on GNOME Shell and will feature new apps and more! Stay tuned for more information."](https://twitter.com/HaNeuersoftware/status/1584529635152252928). _I love the smell of vaporware in the morning! - Just kidding, looking forward for more info!_


### Worth reading
- Martijn Braam: [Automated Phone Testing pt.5](https://blog.brixit.nl/automated-phone-testing-pt-5/)
- Lup Yuen Lee: [Rendering PinePhone's Display (DE and TCON0)](https://lupyuen.github.io/articles/de)
- Purism: [Kerberos authentication on the Librem 5](https://puri.sm/posts/kerberos-authentication-on-the-librem-5/). _[Also on Guido's blog](https://honk.sigxcpu.org/con/Kerberos_authentication_on_the_Librem_5.html)._
- TuxPhones.com: [Vanilla OS offers an innovative, modernized "post-Debian" experience](https://tuxphones.com/vanilla-os-linux-apx-immutable-system-upstream-lightweight-ubuntu/)
- Neil Brown: [RSS as my default web browser (for some stuff)](https://neilzone.co.uk/2022/10/rss-as-my-default-web-browser-for-some-stuff)
- Rafael Caricio: [A brief introduction to GStreamer](https://caricio.com/a-brief-introduction-to-gstreamer/)


### Worth watching
- Jacob David Cunningham: [Pinephone Pro powered docks (USB-C to HDMI with PD)](https://www.youtube.com/watch?v=NrOwj2JVlBU)
- Jacob David Cunningham: [Creating a React PWA with Pinephone Pro](https://www.youtube.com/watch?v=OyR7rdtSMsQ)
- Equareo: [Ubuntu touch fairphone 4](https://www.youtube.com/watch?v=Yst0wTZrDiU)
- Jaack: [Operating System (OS) other than Android & iOS](https://www.youtube.com/watch?v=affEg-6Wu5I)
- agar002: [Synit and SqueakPhone demo, 29 October 2022.](https://www.youtube.com/watch?v=aEfssFQ1X1M)
- u/MarkG_108: [A screen recording of Mobian, done using the program wf-recorder (4 minutes)](https://www.reddit.com/r/pinephone/comments/ybse0l/a_screen_recording_of_mobian_done_using_the/)
- The Linux Experiment: [MANJARO has a BIG PROBLEM](https://www.youtube.com/watch?v=oVlD17OjFAc)

### Thanks

Huge thanks to synQun for contributing to this installment of the Weekly Update and also to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email - or just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)

PPS: This one has (once again) less headlines - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update%2039&body=Less%20or%20more%20headlines%20going%20forward%3F)
