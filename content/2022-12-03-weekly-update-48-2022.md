+++
title = "Weekly GNU-like Mobile Linux Update (48/2022): Plasma Mobile Gear 22.11 and improvements to PinePhone and PinePhone Pro"
date = "2022-12-03T16:30:00Z"
draft = false
[taxonomies]
tags = ["Sailfish OS","Nemo Mobile", "Plasma Mobile", "PINE64 Community Q&A", "Manjaro", "Maemo Leste",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Also: Sailfish OS progress on PINE64 devices, releases of mmsd-tng and vvmplayer, a talk about Maemo Leste and a lot more.

<!-- more -->

_Commentary in italics._


### Software progress

#### Hardare enablement
- megi's PinePhone Development Log: [Pinephone kernel news and some bits about the keyboard, too](https://xnux.eu/log/#076). _Lots of great improvements for PinePhone and PinePhone Pro._

#### GNOME ecosystem
- This Week in GNOME: [#72 Automated Testing](https://thisweek.gnome.org/posts/2022/12/twig-72/)
- GNOME Shell & Mutter: [Automated testing of GNOME Shell](https://blogs.gnome.org/shell-dev/2022/12/02/automated-testing-of-gnome-shell/)
- halting problem: [On PyGObject](https://www.bassi.io/articles/2022/12/02/on-pygobject/)

#### Plasma ecosystem	
- Plasma Mobile: [Plasma Mobile Gear ⚙ 22.11 is Out](https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/)
- Nate Graham: [This week in KDE: custom tiling](https://pointieststick.com/2022/12/02/this-week-in-kde-custom-tiling/)
- dot.kde.org: [KDE's End of Year Fundraiser is Live](https://dot.kde.org/2022/12/02/kdes-end-year-fundraiser-live)
  - Nate Graham: [Help KDE hire more people!](https://pointieststick.com/2022/12/01/help-kde-hire-more-people/)
- KDE Announcemnets: [KDE Plasma 5.26.4, Bugfix Release for November](https://kde.org/announcements/plasma/5/5.26.4/)
- Volker Krause: [October/November in KDE Itinerary](https://www.volkerkrause.eu/2022/12/03/kde-itinerary-october-november-2022.html)

#### Nemo Mobile
- Nemo Mobile UX team: [Advent of code](https://nemomobile.net/pages/advent-of-code/)
- Nemo Mobile UX team: [Nemomobile in November 2022](https://nemomobile.net/pages/nemomobile-in-november-2022/)
- neochapay on twitter: [RT by @neochapay: The elves at the development station have been busy at work, thank you @neochapay @xmlich02 and others! https://nemomobile.net/pages/advent-of-code/](https://twitter.com/NemoMobile/status/1598319345981132801#m)
- neochapay on twitter: [First MR with new UI of #nemomobile  https://github.com/nemomobile-ux/glacier-home/pull/208](https://twitter.com/neochapay/status/1597879534166355969#m)

#### Ubuntu Touch

#### Sailfish OS
- [Sailfish Community News, 1st December, Pine Logs](https://forum.sailfishos.org/t/sailfish-community-news-1st-december-pine-logs/13664)

#### Sxmo

#### Distributions
- Phoronix: [NixOS 22.11 Released With Better AArch64 Support, NVIDIA Open GPU Kernel Driver Option](https://www.phoronix.com/news/NixOS-22.11-Released)
- Manjaro PinePhone Plasma Mobile: [Beta 13](https://github.com/manjaro-pinephone/plasma-mobile/releases/tag/beta13). _Note: This does not contain the latest Plasma Mobile Gear improvements yet._
- Manjaro PinePhone Plasma Mobile: [Release 202211270709](https://github.com/manjaro-pinephone/plasma-mobile/releases/tag/202211270709)

#### Stack
- Phoronix: [SDL Tries Again To Prefer Wayland Over X11](https://www.phoronix.com/news/SDL-3.0-Wayland-Default-Prefer)

#### Non-Linux
- Phoronix: [Genode OS 22.11 Released With Support For Intel Gen12 Graphics, More PinePhone Work](https://www.phoronix.com/news/Genode-OS-22.11)

#### Matrix
- Matrix.org: [This Week in Matrix 2022-12-02](https://matrix.org/blog/2022/12/02/this-week-in-matrix-2022-12-02)
- Matrix.org: [Funding Matrix via the Matrix.org Foundation](https://matrix.org/blog/2022/12/01/funding-matrix-via-the-matrix-org-foundation)

### Worth noting
- Guido keeps working on [his toy project](https://social.librem.one/@agx/109445238070139307) phosh-osk-stub: [Opportunistic completion](https://social.librem.one/@agx/109428599061094716) and [emoji](https://social.librem.one/@agx/109445029220794083) are the highlights this week.
- If Visual Voicemail is your thing, you'll love the [2.0 release of vvmplayer that was tagged this week](https://fosstodon.org/@kop316/109435851694289971).
- kop316 [also released mmsd-tng 2.0](https://social.librem.one/@agx/109445238070139307), for people that love or are forced to use MMS.
- [Librem 5 call audio](https://social.librem.one/@dos/109428257796392532) should now be improved.


### Worth reading
- megi's PinePhone Development Log: [Pinephone kernel news and some bits about the keyboard, too](https://xnux.eu/log/#076). _Just in case you missed it above._
- Purism: [Purism at “Le Capitole du Libre” in France](https://puri.sm/posts/purism-at-le-capitole-du-libre-in-france/)
- Purism: [Special Year End Promotion for Librem 5 USA](https://puri.sm/posts/special-year-end-promotion-for-librem-5-usa/)
- Drew DeVaults blog: [I shall toil at a reduced volume](https://drewdevault.com/2022/12/01/I-shall-toil-quietly.html). _Sad, but I get it._


### Worth listening
* [Late Night Linux – Episode 205](https://latenightlinux.com/late-night-linux-episode-205/). _Nice segment on postmarketOS!_
* [Linux Action News: Linux Action News 269](https://linuxactionnews.com/269). _Ubuntu Touch OTA 24 is the relevant bit in this one._

### Worth watching
- PINE64: [Quarterly Community Q&A [Live Stream]](https://www.youtube.com/watch?v=8yzfyjIz5g0)
- Billy Blackburn: [Using a terminal to browse the web](https://www.youtube.com/watch?v=dv0OnWsmG_4)
- BeeBom: [This Linux Tablet Destroys iPad?](https://www.youtube.com/watch?v=SVMqhZPqxz4) _About the FydeTab Duo._
- V45370: [An alternative to a Raspberry Pi 3 (Lenovo A6000 + PostmarketOS)](https://www.youtube.com/watch?v=F8uuFoHsc8I)
- AgarNoobYT: [Installing Postmarketos on lt023g aka 'Samsung Galaxy tab 3 7.0" - Part 1](https://www.youtube.com/watch?v=5di9vL6PELc)
- Openfest Bulgaria: [An in depth look at Maemo Leste Merlijn Wajer](https://www.youtube.com/watch?v=I2qnjBZ-Scg) _Nice talk!__
- Bryan Lunduke: [A Smalltalk SmartPhone: The "SqueakPhone"](https://www.youtube.com/watch?v=G5bazCrZ5gQ)
- Twinntech: [Give the Pinephone 4 Xmas 2022](https://www.youtube.com/watch?v=F-ZGiudnozA) _Grumpy old man is back, watching videos and commenting them!_[^1]

### Thanks

Huge thanks again to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one! While we were early this week, we will be delayed to Monday next week).


PS: I'm looking for feedback - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update)

[^1]: Honestly, I did not watch this, and you likely should not watch it either.

